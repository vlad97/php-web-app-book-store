<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
  //  return $request->user();
//});

Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');

Route::get('user','UserController@index');

Route::get('user','UserController@index2');

Route::post('user','UserController@store');




Route::get('carti','CartiController@index');


Route::get('carti/{id}','CartiController@show');

Route::post('carti','CartiController@store');

Route::put('carti/{id}', 'CartiController@update');

Route::delete('carti/{id}', 'CartiController@delete');






