<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home','HomeController@index')->name('home');
//Route::get('/user', 'UserController@index')->name('user');
//Route::get('/admin', 'AdminController@index')->name('admin');



//Route::get('/', function () {
   // return view('shop.index');
//});

Route::get('/', [
    'uses' => 'CartiController@getIndex',
    'as' => 'carti.index'
]);



Route::get('/', [
    'uses' => 'CartiController@getIndex',
    'as' => 'carti.index2'
]);

Route::get('/add-to-cart/{id}', [
    'uses' => 'CartiController@getAddToCart',
    'as' => 'carti.addToCart'
]);


Route::get('/reduce/{id}', [
    'uses' => 'CartiController@getReduceByOne',
    'as'  =>  'carti.reduceByOne'
]);

Route::get('/remove/{id}', [
    'uses' => 'CartiController@getRemoveItem',
    'as' => 'carti.remove'

]);

//Route::post('/userii/{id}', [
  //  'uses' => 'UserController@index',
  //  'as' => 'users.index'

//]);

Route::get('/shopping-cart', [
    'uses' => 'CartiController@getCart',
    'as' => 'carti.shoppingCart'
]);

Route::get('/checkout', [
    'uses' => 'CartiController@getCheckout',
    'as' => 'checkout',
    'middleware' => 'auth'
]);

Route::get('/profile', [
   'uses' => 'UserController@getProfile',
   'as' => 'user.profile'
]);

Route::get('/reports', [
    'uses' => 'CartiController@getReports',
'as' => 'shop.reports'
]);

Route::post('/checkout', [
   'uses' => 'CartiController@postCheckout',
    'as' => 'checkout'
]);

//Route::group(['middleware' => ['auth']], function() {

   // Route::resource('carti','CartiController');
//});

Route::resource('carti','CartiController');

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles','RoleController');
    Route::resource('userii','UserController');
    Route::resource('carti','CartiController');
});

Route::middleware(['auth'])->group(function () {
    Route::get('/approval', 'HomeController@approval')->name('approval');
    Route::get('/home', 'HomeController@index')->name('home');
});



Route::middleware(['auth'])->group(function () {
    Route::get('/approval', 'HomeController@approval')->name('approval');

    Route::middleware(['approved'])->group(function () {
        Route::get('/home', 'HomeController@index')->name('home');
    });

    Route::middleware(['admin'])->group(function () {
        Route::get('/users', 'UserController@index2')->name('admin.users.index');
        Route::get('/users/{user_id}/approve', 'UserController@approve')->name('admin.users.approve');
    });
});




