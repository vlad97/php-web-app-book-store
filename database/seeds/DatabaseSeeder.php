<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (\App\User::count()) {
            return;
        }


        $user = new \App\User();
        $user->name = "vlad";
        $user->email = "vlad@mail.com";
        $user->adresa="strada albac";
        $user->password = \Illuminate\Support\Facades\Hash::make("vlad");
        $user->save();

        if(\App\BillingAdress::count())
        {return;}

        $address = new \App\BillingAdress();
        $address->name = "vlad";
        $address->user_id = "1";
        $address->oras = "cluj-napoca";
        $address->strada= "albac nr.6";
        $address->save();



        if (\App\Carti::count()) {
            return;
        }

        $carti= new \App\Carti([

                'titlu' => 'Mara',
                'autor' => 'Ioan Slavici',
                'pret'  => 40,
                'imagePath' => 'https://librariadelfin.ro/site_img/products/400/2014/04/mara_ioan-slavici_herra.jpg',
                'cantitati'  => 50



        ]);
        $carti->save();

          $carti= new \App\Carti([

                  'titlu' => 'Baltagul',
                  'autor' => 'Sadoveanu',
                  'pret'  => 60,
                  'imagePath' => 'https://static.cartepedia.ro/image/192863/baltagul-produs_imagine_new.jpg',
                  'cantitati'  => 500



        ]);
        $carti->save();

        $carti= new \App\Carti([

            'titlu' => 'Ion',
            'autor' => 'Rebreanu',
            'pret'  => 55,
            'imagePath' => 'https://cdn4.libris.ro/img/pozeprod//59/1012/66/878091.jpg',
            'cantitati'  => 350



        ]);
        $carti->save();

        $carti= new \App\Carti([

            'titlu' => 'Poezii',
            'autor' => 'Eminescu',
            'pret'  => 45,
            'imagePath' => 'https://kbimages1-a.akamaihd.net/b7382e0e-1632-467e-bc63-8066cec4301d/353/569/90/False/poezii-mihai-eminescu.jpg',
            'cantitati'  => 700



        ]);
        $carti->save();


        $this->call(RolesTableSeeder::class);

        $this->call(LoginSeeder::class);

        $this->call(AdminSeeder::class);




    }
    }
