<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{

    protected $table = 'cart_items';
    public $timestamps = false;

    public $items = null;
    public $totalCantitati =0;
    public $totalPrice =0;

    public function __construct($oldCart)
    {
        if($oldCart){
            $this->items= $oldCart->items;
            $this->totalCantitati= $oldCart->totalCantitati;
            $this->totalPrice= $oldCart->totalPrice;
        }


    }

    public function add($item, $id){
        $storedItem = ['qty' => 0, 'price' => $item->pret, 'item' => $item];
        if( $this->items)
        {
            if(array_key_exists($id, $this->items)) {
                $storedItem = $this->items[$id];
            }

        }
        $storedItem['qty']++;
        $storedItem['pret'] = $item->pret * $storedItem['qty'];
        $this->items[$id] = $storedItem;
        $this->totalCantitati++;
        $this->totalPrice += $item->pret;
    }

    public function reduceByOne($id){

        $this->items[$id]['qty']--;
        $this->items[$id]['pret'] -= $this->items[$id]['item']['pret'];
        $this->totalCantitati--;
        $this->totalPrice -= $this->items[$id]['item']['pret'];

        if ($this->items[$id]['qty'] <=0){
            unset($this->items[$id]);
        }

    }

    public function removeItem($id){


        $this->totalCantitati-= $this->items[$id]['qty'];
        $this->totalPrice -= $this->items[$id]['pret'];

        unset($this->items[$id]);



    }

    public function getShoppingCart(){
        return $this->belongsTo('App\ShoppingCart','shopping_cart_id');
    }

    public function getBooks(){
        return $this->belongsTo('App\Carti','carti_id');
    }

    public function getOrder(){
        return $this->belongsTo('App\Order','order_id');
    }
}
