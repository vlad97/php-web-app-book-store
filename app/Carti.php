<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Carti extends Model
{

    public $table = 'carti';
    protected $fillable = [
        'titlu', 'autor', 'pret','cantitati','imagePath'
    ];


   // public function users(){
   //     return $this->belongsToMany(User::class,'user_carti');
   // }
}
