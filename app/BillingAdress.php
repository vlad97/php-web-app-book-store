<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class BillingAdress extends Model
{

public $timestamps = false;
protected $table = 'billing_address';

    protected $fillable = [
        'nume','oras','strada',
    ];


public function getUser(){
    return $this->belongsTo('App\User','users_id');
}


}
