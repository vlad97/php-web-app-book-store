<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    public function user(){
        return $this->belongsTo('App\User');
    }

    protected $fillable = [
        'cartitem','name','address'
    ];

    public function getBillingAddress(){
        return $this->belongsTo('App\BillingAddress', 'billing_address_id');
    }
    public function  getUser(){
        return $this->belongsTo('App\User','user_id');
    }
}
