<?php

namespace App\Http\Controllers;
use App\User;

//use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Contracts\Validation\Validator;
//use Validator;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
   public function register(Request $request)
       {
           $validator = Validator::make($request->all(), [
               'name' => 'required',
               'email' => 'required|email',
               'password' => 'required',
               'c_password' => 'required|same:password',
           ]);

   if ($validator->fails()) {
               return response()->json(['error'=>$validator->errors()], 401);
           }
   $input = $request->all();
           // $input['password'] = bcrypt($input['password']);
           $input['password'] = password_hash($input['password'], PASSWORD_BCRYPT, ['cost' => 12]);
         //  $input['deleted'] = false;
           $input['admin'] = false;
           $user = User::create($input);
           $success['token'] =  $user->createToken('MyApp')-> accessToken;
           $success['name'] =  $user->name;
   return response()->json(['success'=>$success], $this-> successStatus);
       }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email',
            'password' => 'required'
        ]);

        if( Auth::attempt(['email'=>$request->email, 'password'=>$request->password]) ) {
            $user = Auth::user();

            $token = $user->createToken($user->email.'-'.now());

            return response()->json([
                'token' => $token->accessToken
            ]);
        }
    }

}
