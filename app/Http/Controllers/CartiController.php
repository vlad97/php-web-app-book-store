<?php

namespace App\Http\Controllers;

use App\CartItem;
use App\Carti;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;


class CartiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Carti[]|\Illuminate\Database\Eloquent\Collection
     */


    public function getIndex(){
        $carti = Carti::all();
        return view('shop.index', ['carti' => $carti]);
    }

    public function getAddToCart(Request $request, $id){
        $carti = Carti::find($id);
        $oldCart = Session::has('cartitem') ? Session::get('cartitem') : null;
        $cartitem = new CartItem($oldCart);
        $cartitem->add($carti, $carti->id);

        $request->session()->put('cartitem', $cartitem );
       // dd($request->session()->get('cartitem'));
        return redirect()->route('carti.index2');



    }

    public function getReduceByOne($id) {
        $oldCart = Session::has('cartitem') ? Session::get('cartitem') : null;
        $cartitem = new CartItem($oldCart);
        $cartitem->reduceByOne($id);

        if(count($cartitem->items)>0)
        {
            Session::put('cartitem', $cartitem);

        }
        else
        {
            Session::forget('cartitem');
        }
        return redirect()->route('carti.shoppingCart');

    }

    public function getRemoveItem($id) {

        $oldCart = Session::has('cartitem') ? Session::get('cartitem') : null;
        $cartitem = new CartItem($oldCart);
        $cartitem->removeItem($id);

        if(count($cartitem->items)>0)
        {
            Session::put('cartitem', $cartitem);

        }
        else
        {
            Session::forget('cartitem');
        }



        return redirect()->route('carti.shoppingCart');


    }


    public function getCart(){
        if(!Session::has('cartitem')){

            return view('shop.shopping-cart');
        }

        $oldCart = Session::get('cartitem');
        $cartitem = new CartItem($oldCart);
        return view('shop.shopping-cart', ['carti' => $cartitem->items, 'totalPrice' => $cartitem->totalPrice]);



    }


    public function getCheckout(){
        if(!Session::has('cartitem')){

            return view('shop.shopping-cart');
        }
        $oldCart = Session::get('cartitem');
        $cartitem = new CartItem($oldCart);
        $total = $cartitem->totalPrice;
        $qty = $cartitem->totalCantitati;
        $titlu= $cartitem->titlu;
        $autor=$cartitem->autor;

        return view('shop.checkout', ['total' => $total],['qty' => $qty]);

        $order= new Order();
        $order->cartitem = serialize($cartitem);
        //$order->address
        Auth::user()->orders()->save($order);



    }



    public function postCheckout(Request $request){

        if(!Session::has('cartitem')){

            return redirect()->route('shop.shoppingCart');
        }
        $oldCart = Session::get('cartitem');
        $cartitem = new CartItem($oldCart);

        $order = new Order();
        $order->cartitem = serialize($cartitem);
        $order->address = $request->input('address');
        $order->name = $request->input('name');
        $order->oras= $request->input('oras');
        $order->cantitati=$request->input('cantitati');
        $order->autor=$request->input('autor');

        Auth::user()->orders()->save($order);

        Session::forget('cartitem');
        return redirect()->route('carti.index2')->with('success', 'Successfully purchased books!');


    }

    public function getReports(Request $request){

        return view ('shop.reports');



    }

   // function __construct()
  //  {
   //     $this->middleware('permission:carti-list|carti-create|carti-edit|carti-delete', ['only' => ['index','show']]);
   //     $this->middleware('permission:carti-create', ['only' => ['create','store']]);
   //     $this->middleware('permission:carti-edit', ['only' => ['edit','update']]);
   //     $this->middleware('permission:carti-delete', ['only' => ['destroy']]);
   // }


    public function index2(){

        return Carti::all();
    }

    public function index()
    {

        $carti = Carti::latest()->paginate(5);
        return view('carti.index',compact('carti'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

       // return Carti::all();
    }

    public function create()
    {
        return view('carti.create');
    }

  //  public function show(Carti $id)
  //  {
   //     return Carti::find($id);

 //   }

    public function show(Carti $carti)
    {
        return view('carti.show', compact('carti'));
    }


    public function edit(Carti $carti)
    {
        return view('carti.edit', compact('carti'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'titlu' => 'required',
            'autor' => 'required',
            'pret' => 'required',
            'cantitati' => 'required',
            'imagePath' => 'required'
        ]);

        Carti::create($request->all());
        return redirect()->route('carti.index')
                        ->with('success','Carte creeata cu succes');


        //$carti = Carti::create($request->all());

       // return response()->json($carti, 201);
    }

  //  public function update(Request $request, $id)
  //  {
  //      $carti = Carti::findOrFail($id);
   //     $carti->update($request->all());

   //     return $carti;
  //  }


    public function update(Request $request, Carti $carti)
    {
        request()->validate([
            'titlu' => 'required',
            'autor' => 'required',
            'pret' => 'required',
            'cantitati' => 'required',
            'imagePath' => 'required'
        ]);

        $carti->update($request->all());

        return redirect()->route('carti.index')
            ->with('success','Carte updated successfully');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function delete(Request $request, $id)
  //  {
   //     $carti = Carti::findOrFail($id);
  //      $carti->delete();

   //     return 204;
  //  }

    public function destroy(Carti $carti)
    {
        $carti->delete();

        return redirect()->route('carti.index')
            ->with('success','Carte deleted successfully');

    }


}
