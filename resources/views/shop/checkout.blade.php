@extends('layouts.master')
@section('title')
    Book Cart
@endsection
@section('content')


    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
            <h1>Checkout</h1>
            <h4>Pretul Total: {{$total}}lei</h4>
            <h4>Cantitati: {{$qty}}</h4>


            <form action="{{route('checkout')}}" method="post" id="checkout-form">
                <div class ="row">
                    <div class="col-xs-12">
                         <div class="form-group">
                             <label for="name">Name</label>
                             <input type="text" id="name" class="form-control" required name="name">
                         </div>
                    </div>
                <div class="col-xs-12">
                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" id="address" class="form-control" required name="address">
                </div>
                </div>
                 <div class="col-xs-12">
                       <div class="form-group">
                              <label for="address">Oras</label>
                          <input type="text" id="oras" class="form-control" required name="oras">
                         </div>
                          </div>

    <div class="col-xs-12">
    <div class="form-group">
        <label for="address">Cantitati</label>
        <input type="text" id="cantitati" class="form-control" required name="cantitati">
    </div>

    </div>
    <div class="col-xs-12">
    <div class="form-group">
        <label for="address">Autor</label>
        <input type="text" id="autor" class="form-control" required name="autor">
    </div>
    </div>

                {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Buy now</button>



            </form>
        </div>
    </div>

@endsection
