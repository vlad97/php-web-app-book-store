@extends('layouts.master')
@section('title')
    Book Cart
@endsection
@section('content')

    @if(Session::has('success'))
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
                <div id="message" class="alert alert-success">
                    {{Session::get('success')}}
         </div>

    @endif
    @foreach($carti->chunk(3) as $cartiChunk)


    <div class="row">
        @foreach($cartiChunk as $carti)


        <div class="col-sm-6 col-md-3">
            <div class="thumbnail">
                <img src="{{ $carti->imagePath }}" alt="..." style="max-height: 150px" class="img-responsive">
                <div class="caption">
                    <h3>{{ $carti->titlu }}</h3>
                    <h4>{{ $carti->autor }}</h4>
                    <div class="clearfix">
                        <div class="float-md-left price">Pret:{{ $carti->pret }}</div>
                        <div class="float-md-left cantitati">Cantitati:{{ $carti->cantitati }}</div>
                        <a href="{{route('carti.addToCart', ['id' =>$carti->id])}}" class="btn btn-success float-md-right" role="button">Add to Cart</a>
                    </div>



                </div>


            </div>



        </div>
        @endforeach

    </div>
    @endforeach
@endsection
