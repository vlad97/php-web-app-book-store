@extends('layouts.master')
@section('title')
    Book Cart
@endsection
@section('content')

    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>

    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">

            <h4> cele mai multe carti pe orase </h4>

            <?php

            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "carti";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);

            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT name, oras,  SUM(cantitati) FROM orders group BY oras ORDER BY COUNT(cantitati) ASC";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {

                echo "<table><tr><th>Name</th><th>Oras</th><th>Cantitati</th></tr>";

                while($row = $result->fetch_assoc()) {


                   // echo "name: " . $row["name"].  " oras: " . $row["oras"]. " cantitati: "  .  $row["cantitati"].  "<br>";
                    echo "<tr><td>" .  $row["name"].  "</td><td>". $row["oras"].  "</td><td>"  . $row["SUM(cantitati)"].  "</td></tr>";




                }

                echo "</table>";


            } else {
                echo "0 results";
            }


            ?>



            <h4> rapoarte de top cumparatori </h4>


            <?php

            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "carti";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);

            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT name, cantitati FROM orders ORDER BY cantitati DESC";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {

                echo "<table><tr><th>Name</th><th>Cantitati</th></tr>";

                while($row = $result->fetch_assoc()) {
                  //  echo "name: " . $row["name"].  " cantitati: " . $row["cantitati"].  "<br>";

                    echo "<tr><td>" .  $row["name"].  "</td><td>". $row["cantitati"].    "</td></tr>";
                }

                echo "</table>";
            } else {
                echo "0 results";
            }


            ?>


            <h4> cei mai bine vanduti autori</h4>


            <?php

            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "carti";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);

            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT autor, cantitati, SUM(cantitati) FROM orders GROUP BY autor ORDER BY COUNT(cantitati) ASC";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {

                echo "<table><tr><th>Autor</th><th>Cantitati</th></tr>";

                while($row = $result->fetch_assoc()) {
                   // echo "autor: " . $row["autor"].  " cantitati: " . $row["cantitati"].  "<br>";
                    echo  "</tr><td>" . $row["autor"].  "</td><td>". $row["SUM(cantitati)"].    "</td></tr>";
                }

                echo "</table>";
            } else {
                echo "0 results";
            }


            ?>


        </div>
    </div>

@endsection
