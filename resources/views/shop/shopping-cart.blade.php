@extends('layouts.master')
@section('title')
    Book Cart
@endsection
@section('content')
    @if(Session::has('cartitem'))
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <ul class="list-group">
                    @foreach($carti as $carte)
                            <li class="list-group-item">
                                <span class="badge">Cantitati:{{ $carte['qty'] }}</span>
                                <strong>Titlu:{{$carte['item']['titlu']}}</strong>
                                <span class="label label-success">Pret: {{$carte['pret']}} </span>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span> </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ route('carti.reduceByOne', ['id' => $carte['item']['id']])}}">Reduce by 1</a></li>
                                        <li><a href="{{ route('carti.remove', ['id' => $carte['item']['id']])}}">Reduce all</a></li>

                                    </ul>
                                </div>
                            </li>
                    @endforeach
                </ul>

            </div>

        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
              <strong>Total:{{$totalPrice}}</strong>

            </div>

        </div>
        <hr>
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <a href="{{route('checkout')}}" type="button" class="btn btn-success">Checkout</a>

            </div>

        </div>

    @else
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <h2>No Items in Cart!</h2>
            </div>

        </div>
        @endif


@endsection
