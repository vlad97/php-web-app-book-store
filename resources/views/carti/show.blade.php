@extends('carti.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Carte</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('carti.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Titlu:</strong>
                {{ $carti->titlu }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Autor:</strong>
                {{ $carti->autor }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <img src="{{ $carti->imagePath }}" alt="..." style="max-height: 150px" class="img-responsive">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Pret:</strong>
                {{ $carti->pret }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Cantitati:</strong>
                {{ $carti->cantitati }}
            </div>
        </div>
    </div>
@endsection
