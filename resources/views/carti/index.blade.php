@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">

            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('carti.create') }}"> Creeaza o noua carte</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Titlu</th>
            <th>Autor</th>
            <th>ImagePath</th>
            <th>Pret</th>
            <th>Cantitati</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($carti as $carte)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $carte->titlu }}</td>
                <td>{{ $carte->autor }}</td>
                <td>{{ $carte->imagePath}}</td>
                <td>{{ $carte->pret }}</td>
                <td>{{ $carte->cantitati }}</td>
                <td>
                    <form action="{{ route('carti.destroy',$carte->id) }}" method="POST">

                        <a class="btn btn-info" href="{{ route('carti.show',$carte->id) }}">Show</a>

                        <a class="btn btn-primary" href="{{ route('carti.edit',$carte->id) }}">Edit</a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $carti->links() !!}

@endsection
