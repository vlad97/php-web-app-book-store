<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>



    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{route('carti.index2')}}">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('carti.shoppingCart')}}">Book Cart
                <span class="badge">{{Session::has('cartitem') ? Session::get('cartitem')->totalCantitati : ''}}</span>
                </a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    User Actions
                </a>

                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{'register'}}">Register Account</a>
                    <a class="dropdown-item" href="{{'login'}}">Login && Logout</a>
                    <a class="dropdown-item" href="{{route('user.profile')}}">My Account</a>



                </div>
            </li>




            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Admin Panel
                </a>

                <div class="dropdown-menu" aria-labelledby="navbarDropdown">




                    @if(!Auth::guest())
                        @hasrole("admin")
                        <a class="nav-link" href="{{ route('userii.index') }}">Manage Users</a>
                        <a class="nav-link" href="{{ route('carti.index') }}">Manage Carti</a>
                        <a class="nav-link" href="{{ route('shop.reports') }}">Rapoarte</a>
                        @endhasrole
                    @endif
                    <div class="dropdown-divider"></div>

                </div>
            </li>

        </ul>

    </div>
</nav>
