@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>User Profile</h1>
            <hr>
            <h2>My Orders</h2>
            @foreach($orders as $order)
            <div class="panel panel-default">
                <div class="panel-body">
                    <ul class="list-group">
                        @foreach($order->cartitem->items as $item)
                        <li class="list-group-item">
                            <span class="badge">{{$item['price']}} lei</span>
                            {{$item['item']['titlu']}} | {{$item['qty']}} bucati
                        </li>
                           @endforeach

                    </ul>


                </div>
                <div class="panel-footer">
                    <strong>Pretul total: lei{{$order->cartitem->totalPrice}}</strong>
                </div>




            </div>
            @endforeach



        </div>







    </div>
@endsection
